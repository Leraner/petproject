from rest_framework.viewsets import ModelViewSet

from users.models import MySiteCustomUser
from users.serializers import MySiteCustomUserSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response


class MySiteCustomUserViewSet(ModelViewSet):
    serializer_class = MySiteCustomUserSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ("username",)

    def get_queryset(self):
        return MySiteCustomUser.objects.all()

    @action(detail=False, permission_classes=[IsAuthenticated])
    def profile(self, request):
        serializer = self.get_serializer(instance=request.user)
        return Response(serializer.data)
