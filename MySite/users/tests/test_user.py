import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken
from rest_framework.response import Response

from users.models import MySiteCustomUser

pytestmark = [
    pytest.mark.django_db,
]


def register(api_client, user_data) -> Response:
    url = reverse('mysitecustomuser-list')
    response = api_client.post(url, user_data, format='json')
    return response


def login(api_client, user_data) -> Response:
    url = reverse('token_obtain_pair')
    response = api_client.post(url, user_data, format='json')
    return response


def test_user_create(api_client, user_data):
    url = reverse('mysitecustomuser-list')
    response = api_client.post(url, user_data, format='json')

    assert response.status_code == status.HTTP_201_CREATED
    assert MySiteCustomUser.objects.count() == 1
    assert response.data['username'] == user_data['username']
    assert response.data['email'] == user_data['email']


def test_delete_user(api_client, create_user):
    user = create_user

    api_client.force_authenticate(user=user)

    url = reverse('users-detail', kwargs={"pk": user.id})
    response = api_client.delete(url, format='json')

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert MySiteCustomUser.objects.count() == 0
    assert MySiteCustomUser.objects.filter(id=user.id).first() is None


def test_update_user(api_client, create_user):
    user = create_user
    api_client.force_authenticate(user=user)

    data = {
        'username': 'TestUsernameUPDATED',
        'email': 'testemailUPDATED@email.ru'
    }

    url = reverse('users-detail', kwargs={"pk": user.id})
    response = api_client.patch(url, data, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert MySiteCustomUser.objects.count() == 1
    assert response.data['username'] == data['username']
    assert response.data['email'] == data['email']


def test_register_user(api_client, user_data):
    response = register(api_client, user_data)

    assert response.status_code == status.HTTP_201_CREATED
    assert response.data['email'] == user_data['email']
    assert response.data['username'] == user_data['username']
    assert MySiteCustomUser.objects.count() == 1


def test_login_user(api_client, user_data):
    url = reverse('mysitecustomuser-list')
    response = api_client.post(url, user_data, format='json')
    assert response.status_code == status.HTTP_201_CREATED

    response = login(api_client, user_data)
    assert response.status_code == status.HTTP_200_OK


def test_logout_user(api_client, user_data):
    response = register(api_client, user_data)
    assert response.status_code == status.HTTP_201_CREATED

    response = login(api_client, user_data)
    assert response.status_code == status.HTTP_200_OK

    data = {'refresh': response.data['refresh']}

    url = reverse('token_logout')
    response = api_client.post(url, data, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 0
    assert OutstandingToken.objects.count() == 1
    assert OutstandingToken.objects.filter(id=1).first().token == data['refresh']
