from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from users.models import MySiteCustomUser


class MySiteCustomUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = MySiteCustomUser
        fields = [
            'id',
            'status',
            'username',
            'email',
            'image',
        ]
        validators = [
            UniqueTogetherValidator(
                queryset=MySiteCustomUser.objects.all(),
                fields=['username', 'email', ]
            )
        ]
