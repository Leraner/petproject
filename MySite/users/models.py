from django.db import models
from django.contrib.auth.models import AbstractUser


class MySiteCustomUser(AbstractUser):
    """Custom user model"""
    status = models.CharField(max_length=30)
    image = models.ImageField(upload_to='profile/%Y/%m/%d/', null=True, blank=True)

