import pytest
from rest_framework.test import APIClient

from django.conf import settings
from comments.models import Comment
from posts.models import Post, Category
from users.models import MySiteCustomUser


@pytest.fixture(autouse=True)
def override_settings():
    settings.CHANNEL_LAYERS = {
        "default": {
            "BACKEND": "channels.layers.InMemoryChannelLayer"
        }
    }


@pytest.fixture(scope='function')
def api_client():
    return APIClient()


@pytest.fixture(scope='function')
def user_data():
    return {
        "username": "pytestusername",
        "password": "pytestpassword123",
        "email": "pytest@email.ru",
        "status": "",
        "image": None,
    }


@pytest.fixture(scope='function')
def category_data():
    return [
        {
            "title": "Other",
            "slug": "other",
        },
        {
            "title": "Game",
            "slug": "game"
        }
    ]


@pytest.fixture(scope='function')
def post_data(create_category):
    return {
        "title": "test_title",
        "body": "test_body",
        "category": create_category[0]
    }


@pytest.fixture(scope='function')
def comment_data(create_post):
    return {
        "body": "test_body",
        "author": create_post.author,
        "post": create_post
    }


@pytest.fixture(scope='function')
def create_user(user_data):
    return MySiteCustomUser.objects.create(**user_data)


@pytest.fixture(scope='function')
def create_post(create_user, post_data):
    return Post.objects.create(author=create_user, **post_data)


@pytest.fixture(scope='function')
def create_category(category_data):
    categories = []
    for data in category_data:
        categories.append(Category.objects.create(**data))

    return categories


@pytest.fixture(scope='function')
def create_comment(comment_data):
    return Comment.objects.create(**comment_data)
