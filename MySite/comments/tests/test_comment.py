import json

import pytest
from django.urls import reverse
from rest_framework import status

from comments.models import Comment

pytestmark = [
    pytest.mark.django_db,
]


def test_get_comment(api_client, create_user, create_comment, comment_data):
    user = create_user
    api_client.force_authenticate(user=user)
    url = reverse('comments-list')
    response = api_client.get(url)

    content = json.loads(response.content.decode())['results'][0]

    assert response.status_code == status.HTTP_200_OK
    assert content['body'] == comment_data['body']
    assert content['post'] == comment_data['post'].id
    assert content['author']['username'] == comment_data['author'].username


def test_create_comment(api_client, create_user, create_category, create_post):
    user = create_user
    post = create_post

    api_client.force_authenticate(user=user)

    comment_data = {
        "author": {
            "id": user.id,
            "username": 'admin',
            "email": user.email
        },
        "body": "Test comment body",
        "post": post.id,
        "parent": None,
        "children": []
    }

    url = reverse('comments-list')

    response = api_client.post(url, data=comment_data, format='json')

    assert response.status_code == status.HTTP_201_CREATED
    assert response.data['body'] == comment_data['body']
    assert response.data['post'] == create_post.id
    assert response.data['author']['id'] == user.id


def test_update_comment(api_client, create_comment, create_user, create_post):
    user = create_user
    comment = create_comment
    post = create_post

    new_comment_data = {
        "author": {
            "id": user.id,
            "username": 'admin',
            "email": user.email
        },
        "body": "Updated comment body",
        "post": post.id,
        "parent": None,
        "children": []
    }

    api_client.force_authenticate(user=user)
    url = reverse('comments-detail', kwargs={'pk': comment.id})

    assert Comment.objects.filter(id=comment.id).first().body != new_comment_data['body']

    response = api_client.put(url, data=new_comment_data, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert response.data['body'] == new_comment_data['body']
    assert response.data['post'] == new_comment_data['post']


def test_patch_comment(api_client, create_comment, create_user, create_post):
    user = create_user
    post = create_post
    comment = create_comment

    new_comment_data = {
        "author": {
            "id": user.id,
            "username": 'admin',
            "email": user.email
        },
        "body": "Patched body"
    }

    api_client.force_authenticate(user=user)
    url = reverse('comments-detail', kwargs={'pk': comment.id})

    assert Comment.objects.filter(id=comment.id).first().body != new_comment_data['body']

    response = api_client.patch(url, data=new_comment_data, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert response.data['body'] == new_comment_data['body']
    assert response.data['post'] == post.id


def test_delete_comment(api_client, create_comment, create_user, create_post):
    user = create_user
    post = create_post
    comment = create_comment

    comment_data = {
        "author": {
            "id": user.id,
            "username": 'admin',
            "email": user.email
        },
        "body": "Test comment body",
        "post": post.id,
        "parent": None,
        "children": []
    }

    api_client.force_authenticate(user=user)
    url = reverse('comments-detail', kwargs={'pk': comment.id})

    assert Comment.objects.count() == 1

    response = api_client.delete(url, data=comment_data, format='json')

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Comment.objects.count() == 0
    assert response.data is None
