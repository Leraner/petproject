from abc import ABC

from rest_framework import serializers

from comments.models import Comment
from users.serializers import MySiteCustomUserSerializer


class RecursiveCommentSerializer(serializers.Serializer):
    """Creating recursion for all comments"""

    def to_representation(self, instance):
        serializer = self.parent.parent.__class__(instance, context=self.context)
        return serializer.data


class FilterReviewListSerializer(serializers.ListSerializer):
    """Filtering comments which with parent=None"""

    def to_representation(self, data):
        data = filter(lambda instance: instance.parent is None, data)
        # data = data.filter(parent=None)
        return super().to_representation(data)


class CommentSerializer(serializers.ModelSerializer):
    """Serializer for comments"""

    author = MySiteCustomUserSerializer(read_only=True)
    children = RecursiveCommentSerializer(many=True, read_only=True)

    class Meta:
        list_serializer_class = FilterReviewListSerializer
        model = Comment
        fields = (
            "author",
            "body",
            "created",
            "edited",
            "children",
            "post",
            "parent",
        )
