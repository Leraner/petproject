from django.db import models

from posts.models import Post
from users.models import MySiteCustomUser
from mptt.models import MPTTModel, TreeForeignKey


class Comment(MPTTModel):
    """Comment model"""
    body = models.CharField(max_length=150)
    # on_delete = models.SET_NULL
    parent = TreeForeignKey('self', related_name='children', on_delete=models.CASCADE, null=True, blank=True)
    # on_delete = models.SET_NULL
    author = models.ForeignKey(MySiteCustomUser, on_delete=models.CASCADE)
    # on_delete = models.SET_NULL
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='children')
    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.author.username}: {self.author.id}'

    def MPTTMeta(self):
        order_insertion_by = ['-created']
