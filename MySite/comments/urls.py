from rest_framework.routers import DefaultRouter

from comments.views import CommentView

router = DefaultRouter()
router.register('', CommentView, basename='comments')

urlpatterns = [
]

urlpatterns += router.urls
