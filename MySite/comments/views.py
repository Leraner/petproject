from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet
from comments.models import Comment
from comments.serializers import CommentSerializer
from posts.mixins import GettingInformationFromDB
from django_filters.rest_framework import DjangoFilterBackend
from .permissions import IsOwnerOrStaffOrReadOnly


class CommentPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 15


class CommentView(
    GettingInformationFromDB,
    ModelViewSet
):
    pagination_class = CommentPagination
    serializer_class = CommentSerializer
    filter_backends = [DjangoFilterBackend]
    permission_classes = [IsOwnerOrStaffOrReadOnly]
    filter_fields = ("post",)

    def get_queryset(self):
        queryset = Comment.objects.all()
        return queryset

    def perform_update(self, serializer):
        self._get_author(serializer)
        serializer.save()

    def perform_create(self, serializer):
        self._get_author(serializer)
        serializer.save()
