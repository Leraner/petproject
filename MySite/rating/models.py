from django.db import models

from users.models import MySiteCustomUser


class RatingFields(models.Model):
    """Rating model"""
    liked_users = models.ManyToManyField(MySiteCustomUser, related_name='liked_%(class)ss', blank=True)
    disliked_users = models.ManyToManyField(MySiteCustomUser, related_name='disliked_%(class)ss', blank=True)
    rating = models.IntegerField('Rating', blank=True, default=0)

    class Meta:
        abstract = True  # Doesn't create database table. It is used as a base class for other model
