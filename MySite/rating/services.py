from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


class RatingActionsMixin:

    @action(detail=True, methods=['POST'], permission_classes=[IsAuthenticated])
    def like(self, request, pk=None):
        instance = self.get_object()
        rating_handler(request.user, instance, instance.liked_users, instance.disliked_users)
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance)
        return Response(serializer.data)

    @action(detail=True, methods=['POST'], permission_classes=[IsAuthenticated])
    def dislike(self, request, pk=None):
        instance = self.get_object()
        rating_handler(request.user, instance, instance.disliked_users, instance.liked_users)
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance)
        return Response(serializer.data)


def rating_handler(user, instance, current, other):
    """
    current, other - actions (like or dislike)
    if current already exists: remove it (cancel action).
    otherwise add a request user to current action. other remove.
    """
    current_exists = current.filter(id=user.id).first()
    other_exists = other.filter(id=user.id).first()

    current.remove(user) if current_exists else current.add(user)
    other.remove(user) if other_exists else None

    instance.rating = instance.liked_users.count() - instance.disliked_users.count()
    instance.save()
