from django.core.management.base import BaseCommand, CommandError

from posts.models import Category


class Command(BaseCommand):
    help = 'Create prepare stand for project'
    CATEGORIES = {"other": "Other", "game": "Game"}

    def handle(self, *args, **options):
        for category in self.CATEGORIES:
            Category.objects.create(slug=category, title=self.CATEGORIES[category])
            print(f'Создана категория {self.CATEGORIES[category]}')
