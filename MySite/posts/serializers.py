from rest_framework import serializers
from taggit_serializer.serializers import (
    TagListSerializerField,
    TaggitSerializer
)

from posts.models import Post
from users.serializers import MySiteCustomUserSerializer


class PostSerializer(TaggitSerializer, serializers.ModelSerializer):
    author = MySiteCustomUserSerializer(read_only=True)
    category = serializers.SlugRelatedField(slug_field='slug', read_only=True)
    tags = TagListSerializerField(required=False)
    likes = serializers.ReadOnlyField()
    dislikes = serializers.ReadOnlyField()

    class Meta:
        model = Post
        fields = [
            'id',
            'title',
            'body',
            'image',
            'author',
            'category',
            'tags',
            'publish_date',
            'edit_date',
            'likes',
            'dislikes',
        ]
