from django.db import models
from taggit.managers import TaggableManager

from rating.models import RatingFields
from users.models import MySiteCustomUser


class Category(models.Model):
    """Category model"""
    title = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.title


def get_default_category():
    return Category.objects.filter(slug='other').first()


class Post(RatingFields):
    """Post model"""
    title = models.CharField(max_length=200)
    body = models.TextField(max_length=1000)
    image = models.ImageField(upload_to='cover/%Y/%m/%d/', null=True, blank=True)
    author = models.ForeignKey(MySiteCustomUser, on_delete=models.CASCADE, null=False, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=get_default_category,
                                 related_name='post_category', null=False, blank=False)
    tags = TaggableManager(blank=True)
    publish_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.title}'

    def delete(self, *args, **kwargs):
        if self.image:
            self.image.delete()

        super().delete(*args, **kwargs)
