from users.models import MySiteCustomUser
from rest_framework.exceptions import ValidationError
from posts.models import Category


class GettingInformationFromDB:
    """
    Mixin that find user and category, where we need to use it, while object is creating.
    """

    def _get_category(self, serializer) -> None:
        category = Category.objects.filter(slug=self.request.data['category']).first()

        if category is not None:
            serializer.validated_data['category'] = category

    def _get_author(self, serializer) -> None:
        if self.request.data.get('author') is None:
            raise ValidationError({"author": ["This field is required."]})

        author = MySiteCustomUser.objects.filter(id=self.request.data['author']['id']).first()

        if author is None:
            raise ValidationError({"author": ["Wrong meaning"]})

        serializer.validated_data['author'] = author
