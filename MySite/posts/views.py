from django.db.models import Count
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet

from posts.models import Post
from posts.permissions import IsOwnerOrStaffOrReadOnly
from posts.serializers import PostSerializer
from rating.services import RatingActionsMixin
from posts.mixins import GettingInformationFromDB


class PostPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 100


class PostViewSet(
        RatingActionsMixin,
        GettingInformationFromDB,
        ModelViewSet
):
    serializer_class = PostSerializer
    pagination_class = PostPagination
    filter_backends = [DjangoFilterBackend, SearchFilter]

    filterset_fields = ['category__slug']
    search_fields = ['author__username']

    permission_classes = [IsOwnerOrStaffOrReadOnly]

    def get_queryset(self):
        return Post.objects.all().annotate(
            likes=Count('liked_users', distinct=True),
            dislikes=Count('disliked_users', distinct=True)
        )

    def perform_update(self, serializer):
        if self.request.data.get('category'):
            self._get_category(serializer)

        serializer.validated_data['edit_date'] = timezone.now()
        serializer.save()

    def perform_create(self, serializer):
        self._get_author(serializer)

        if self.request.data.get('category'):
            self._get_category(serializer)

        serializer.save()
