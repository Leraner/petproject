import json

import pytest
from django.urls import reverse
from rest_framework import status

from posts.models import Post

pytestmark = [
    pytest.mark.django_db,
]


def test_post_get(api_client, create_user, create_post, post_data) -> None:
    user = create_user
    api_client.force_authenticate(user=user)

    url = reverse('posts-list')

    response = api_client.get(path=url)

    content = json.loads(response.content.decode())['results'][0]

    assert response.status_code == status.HTTP_200_OK
    assert post_data['title'] == content['title']
    assert post_data['body'] == content['body']


def test_post_create(api_client, create_category, create_user) -> None:
    user = create_user
    api_client.force_authenticate(user=user)

    data = {
        "title": "test_title",
        "body": "test_body",
        "author": {
            "id": user.id,
            "username": user.username,
            "email": user.email,
            "status": "",
            "image": None,
        },
        "tags": [
            "testtag",
        ]
    }

    url = reverse('posts-list')

    assert Post.objects.count() == 0

    response = api_client.post(url, data=data, format='json')

    print(response.data)
    assert response.status_code == status.HTTP_201_CREATED
    assert Post.objects.count() == 1
    assert response.data['title'] == data['title']
    assert response.data['body'] == data['body']
    assert response.data['author'] == data['author']
    assert response.data['category'] == 'other'
    assert response.data['tags'] == data['tags']


def test_post_update(api_client, create_category, create_user, create_post) -> None:
    user = create_user
    post = create_post

    api_client.force_authenticate(user=user)

    data = {
        "title": "test_title2",
        "body": "test_body2",
        "category": "game",
        "tags": [
            "test",
        ]
    }

    url = reverse('posts-detail', kwargs={'pk': post.id})

    response = api_client.put(url, data=data, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert response.data['title'] == data['title']
    assert response.data['body'] == data['body']
    assert response.data['category'] == data['category']
    assert response.data['tags'] == data['tags']


def test_post_delete(api_client, create_user, create_post) -> None:
    user = create_user
    post = create_post
    api_client.force_authenticate(user=user)

    assert Post.objects.count() == 1

    url = reverse('posts-detail', kwargs={'pk': post.id})
    response = api_client.delete(url, format='json')

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Post.objects.count() == 0
