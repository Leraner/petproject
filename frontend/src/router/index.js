import { createRouter, createWebHistory } from "vue-router";
import store from "../store";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue"),
  },
  {
    path: "/signup",
    name: "Signup",
    component: () => import("../views/Signup.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/post/:postId",
    name: "PostDetail",
    props: true,
    component: () => import("../components/post/PostDetail.vue"),
  },
  {
    path: "/post/edit/:postId",
    name: "PostEdit",
    props: true,
    component: () => import("../components/post/PostEdit.vue"),
    meta: {
      requiredLogin: true,
    },
  },
  {
    path: "/post/create",
    name: "PostCreate",
    component: () => import("../components/post/PostCreate.vue"),
    meta: {
      requiredLogin: true,
    },
  },
  {
    path: "/post/getuserpost/:username",
    name: "MyPosts",
    props: true,
    component: () => import("../components/post/PostList.vue"),
    meta: {
      requiredLogin: true,
    },
  },
  {
    path: "/profile",
    name: "MyProfile",
    component: () => import("../components/profile/MyProfile.vue"),
    meta: {
      requiredLogin: true,
    },
  },
  {
    path: "/profile/:username",
    name: "Profile",
    props: true,
    component: () => import("../components/profile/Profile.vue"),
  },
  {
    path: "/profile/edit",
    name: "EditMyProfile",
    component: () => import("../components/profile/EditProfile.vue"),
    meta: {
      requiredLogin: true,
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if (
    to.matched.some((record) => record.meta.requiredLogin) &&
    !store.state.isAuthenticated
  ) {
    next({ name: "Login", query: { to: to.path } });
  } else {
    next();
  }
});

export default router;
