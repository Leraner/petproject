import { createStore } from "vuex";
import axios from "axios";
import post from "../components/post/store/index";
import comment from "../components/comment/store/index";
import profile from "../components/profile/store/index";
import router from "../router";

export default createStore({
  state: {
    currentPost: null,
    isAuthenticated: false,
    accessToken: "",
    refreshToken: "",
    isLoading: false,
    user: {
      username: "",
      id: null,
      email: "",
    },
  },
  mutations: {
    initializeBlog(state) {
      if (
        localStorage.getItem("accessToken") ||
        localStorage.getItem("refreshToken")
      ) {
        state.accessToken = localStorage.getItem("accessToken");
        state.refreshToken = localStorage.getItem("refreshToken");
        state.isAuthenticated = true;
      } else {
        state.accessToken = "";
        state.refreshToken = "";
        state.isAuthenticated = false;
      }
    },
    setIsLoading(state, status) {
      state.isLoading = status;
    },
    setToken(state, data) {
      if (data.refresh || data.access) {
        axios.defaults.headers.common["Authorization"] =
          "Bearer " + data.access;
        state.accessToken = data.access;
        state.refreshToken = data.refresh;
        state.isAuthenticated = true;
      }
    },
    setUser(state, data) {
      state.user.username = data.username;
      state.user.id = data.id;
      state.user.email = data.email;
    },
    removeToken(state) {
      localStorage.removeItem("accessToken");
      localStorage.removeItem("refreshToken");
      state.accessToken = "";
      state.refreshToken = "";
      state.isAuthenticated = false;
    },
  },
  actions: {
    async signup(context, payload) {
      const { status, data } = await axios.post("/auth/users/", payload);

      let result = { success: true, data, status };

      if (status === 201) {
        return { ...result };
      }
    },
    async logout({ commit, state }) {
      let data = {};
      if (state.refreshToken) {
        const { status } = await axios.post("users/api/token/logout/", {
          refresh: state.refreshToken,
        });
        data = { status };
      }

      if (data.status === 200) {
        commit("removeToken");
      }

      if (!state.isAuthenticated) {
        router.go("/login");
      }
    },
    async login({ commit, state }, payload) {
      const { status, data } = await axios.post("/users/api/token/", payload);

      if (status !== 200) {
        console.log(status, "Error login!!");
      }

      if (data.access && data.refresh) {
        localStorage.setItem("accessToken", data.access);
        localStorage.setItem("refreshToken", data.refresh);
        commit("setToken", data);
      }

      if (state.isAuthenticated) {
        return { success: true, status };
      }
    },
    async getNewTokens({ commit }) {
      const refreshToken = localStorage.getItem("refreshToken");

      if (!refreshToken) {
        commit("removeToken");
        router.go("/login");
        return;
      }

      const formData = {
        refresh: refreshToken,
      };

      const { status, data } = await axios.post(
        "/users/api/token/refresh/",
        formData
      );

      if (status === 200) {
        localStorage.setItem("accessToken", data.access);
        const tokensData = {
          refresh: refreshToken,
          access: data.access,
        };
        commit("setToken", tokensData);
      }
    },
  },
  getters: {
    AuthState(state) {
      return state.isAuthenticated;
    },
  },
  modules: {
    post,
    comment,
    profile,
  },
});
