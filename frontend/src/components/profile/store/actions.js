import axios from "axios";

export default {
  async updateMyProfile(context, payload) {
    const { status, data } = await axios.put(
      `/users/${payload.id}/`,
      payload.profile
    );

    let result = { success: true, status, data };

    if (status === 200) {
      return { ...result };
    }
  },
  async deleteProfile(context, payload) {
    const { status } = await axios.delete(`/users/${payload}/`);

    let result = { success: true, status };

    if (status === 204) {
      return { ...result };
    }
  },
  async fetchProfile(context, payload) {
    const { data, status } = await axios.get(
      `http://127.0.0.1:8000/users/?username=${payload}`
    );

    let result = { success: true, data, status };

    if (status === 200) {
      return { ...result };
    }
  },
  async fetchMyProfile() {
    const { data, status } = await axios.get(`users/profile/`);

    let result = { success: true, data, status };

    if (status === 200) {
      return { ...result };
    }
  },
};
