import axios from "axios";

export default {
  async sendDisLike(context, payload) {
    const { status, data } = await axios.post(
      `/api/posts/${payload.id}/dislike/`,
      payload
    );

    let result = { success: true, data };

    if (status === 200) {
      return { ...result };
    }
  },
  async sendLike(context, payload) {
    const { status, data } = await axios.post(
      `/api/posts/${payload.id}/like/`,
      payload
    );

    let result = { success: true, data };

    if (status === 200) {
      return { ...result };
    }
  },
  async updatePost(context, payload) {
    const { status, data } = await axios.put(
      `api/posts/${payload.postID}/`,
      payload.post
    );

    let result = { success: true, status, data };

    if (status === 200) {
      return { ...result };
    }
  },
  async deletePost(context, payload) {
    const { status } = await axios.delete(`api/posts/${payload}/`);

    let result = { success: true, status, postId: payload };

    if (status === 204) {
      return { ...result };
    }
  },
  async getPostList(context, payload = null) {
    let result = {};

    if (payload) {
      const response = await axios.get(`api/posts/?search=${payload}`);
      result = { data: response.data, status: response.status, success: true };
    } else {
      const response = await axios.get("/api/posts/");
      result = { data: response.data, status: response.status, success: true };
    }

    if (result.status === 200) {
      return { ...result };
    }
  },
  async getPostByUsername(context, payload) {
    const { status, data } = await axios.get(`api/posts/?search=${payload}`);

    let result = { success: true, data, status };

    if (status === 200) {
      return { ...result };
    }
  },
  async getPost(context, payload) {
    const { status, data } = await axios.get(`api/posts/${payload}`);

    let result = { success: true, data, status };

    if (status === 200) {
      return { ...result };
    }
  },
  async getPostNextPage(context, payload) {
    const { status, data } = await axios.get(payload);

    let result = { success: true, data, status };

    if (status === 200) {
      return { ...result };
    }
  },
  async createPost(context, payload) {
    const { status, data } = await axios.post("/api/posts/", payload);

    let result = { success: true, data, status };

    if (status === 201) {
      return { ...result };
    }
  },
};
