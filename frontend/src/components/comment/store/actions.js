import axios from "axios";

export default {
  async fetchCommentsList(context, payload) {
    const { status, data } = await axios.get(`api/comments/?post=${payload}`);

    let result = { success: true, data, status };

    if (status === 200) {
      return { ...result };
    }
  },
  async createComment(context, payload) {
    const { status, data } = await axios.post("api/comments/", payload);

    let result = { success: true, data };

    if (status === 201) {
      return { ...result };
    }
  },
};
